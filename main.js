(() => {
    /**
     * Check and set a global guard variable.
     * If this content script is injected into the same page again,
     * it will do nothing next time.
     */
    if (window.hasRun) { return }
    window.hasRun = true;



    /**
     * Listen to popup message
     */
    browser.runtime.onMessage.addListener((message) => {

        // if it get that message!
        if (message.command === "blow") {
            // do something
        }

    });

})();
